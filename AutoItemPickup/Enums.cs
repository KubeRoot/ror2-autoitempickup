namespace AutoItemPickup.Enums;

public enum Cause
{
    Teleport,
    Drop
}

public enum Mode
{
    Sequential,
    Random,
    Closest,
    LeastItems
}